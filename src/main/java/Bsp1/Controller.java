package Bsp1;

import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

public class Controller {

    @FXML
    private Pane p1;

    @FXML
    private StackPane s1;

    @FXML
    private Rectangle r1;

    @FXML
    private Label l1;
    private  RotateTransition rotate;
    @FXML
    public void initialize() {
         rotate = new RotateTransition();
        s1.addEventHandler(MouseEvent.MOUSE_CLICKED, this::mouseClick);
    }
    private void mouseClick(MouseEvent mouseEvent) {

        if (mouseEvent.getButton() == MouseButton.PRIMARY) {

            rotate.setByAngle(180);

            rotate.setCycleCount(2);
            rotate.setAutoReverse(true);
            rotate.setNode(s1);
            rotate.play();

        }

    }
}


