package Bsp1;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;

public class Combiner extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/layout/mouse.fxml")));
        stage.setTitle("MouseEvent/Listener");
        stage.setScene(new Scene(root));
        stage.show();

    }
}
