package Bsp2;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.Objects;

public class App extends Application {


    public void start(Stage primaryStage) {
        int BOARD_SIZE = 8;
        int CELL_SIZE = 80;

        Color[] colors = { Color.WHITE, Color.GRAY };
        GridPane board = new GridPane();
        for (int row = 0; row < BOARD_SIZE; row++) {
            for (int col = 0; col < BOARD_SIZE; col++) {
                int colorIndex = (row + col) % 2;
                Rectangle rect = new Rectangle(CELL_SIZE, CELL_SIZE, colors[colorIndex]);
                rect.setOnMouseClicked(e -> {
                    if (rect.getFill() == Color.RED) {
                        rect.setFill(colors[(board.getRowIndex(rect) + board.getColumnIndex(rect)) % 2]);
                    } else {
                        rect.setFill(Color.RED);
                    }
                });
                board.add(rect, col, row);
            }
        }

        board.setAlignment(Pos.CENTER);

        Scene scene = new Scene(board);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Schachbrett");
        primaryStage.show();
    }




}